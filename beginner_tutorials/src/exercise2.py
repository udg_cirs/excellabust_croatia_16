#!/usr/bin/env python

import rospy
from auv_msgs.msg import NavSts, BodyVelocityReq, GoalDescriptor
from beginner_tutorials.srv import Turn, TurnResponse

class Example:
    def __init__(self, name):
        # Save node name
        self.name = name 

        # Save last yaw (initially 0.0)
        self.current_yaw = 0.0

        # Create publisher
        self.pub_bvr = rospy.Publisher("/cola2_control/body_velocity_req",
                                       BodyVelocityReq,
                                       queue_size = 1)

        # Create subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts",
                         NavSts,
                         self.update_nav_sts,
                         queue_size = 1)

        # Create a service
        rospy.loginfo('%s: Create service /perform_rotation', self.name)
        self.perform_full_rotation_srv = rospy.Service('/perform_rotation',
                                                       Turn,
                                                       self.perform_rotation)


    def update_nav_sts(self, msg):
        # Save current yaw
        self.current_yaw = msg.orientation.yaw


    def perform_rotation(self, req):

        # Create WorldWaypointReq msg
        bvr = BodyVelocityReq()
        bvr.header.frame_id = 'sparus2'
        bvr.goal.priority = GoalDescriptor.PRIORITY_NORMAL
        bvr.goal.requester = self.name
        bvr.goal.id = 0
        bvr.twist.angular.z = req.yaw_speed
        bvr.disable_axis.x = True
        bvr.disable_axis.y = True
        bvr.disable_axis.z = True
        bvr.disable_axis.roll = True
        bvr.disable_axis.pitch = True
        bvr.disable_axis.yaw = False

        for i in range(req.time_in_seconds * 10):
            # Publish topic
            bvr.header.stamp = rospy.Time.now()
            self.pub_bvr.publish(bvr)
            rospy.sleep(0.1)

        ret = TurnResponse()
        ret.final_yaw = self.current_yaw
        return ret


if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('service_example')

        # Instatiated the class
        NC = Example(rospy.get_name())

        # Wait
        rospy.spin()
    except rospy.ROSInterruptException:
        pass

