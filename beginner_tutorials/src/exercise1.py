#!/usr/bin/env python

# ROS imports
import roslib
import rospy
import math
from cola2_lib import cola2_lib
from auv_msgs.msg import NavSts, WorldWaypointReq, GoalDescriptor

class Example:
    def __init__(self, name):
        # Save node name
        self.name = name 

        # Create subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts",
                         NavSts,
                         self.update_nav_sts,
                         queue_size = 1)

        # Create publisher
        self.pub_wwr = rospy.Publisher("/cola2_control/world_waypoint_req",
                                       WorldWaypointReq,
                                       queue_size = 1)

    def update_nav_sts(self, msg):
        # Save current yaw
        new_yaw = cola2_lib.normalizeAngle( msg.orientation.yaw + math.pi/4 )

        # Create WorldWaypointReq msg
        wwr = WorldWaypointReq()
        wwr.header.stamp = rospy.Time.now()
        wwr.header.frame_id = 'world'
        wwr.goal.priority = GoalDescriptor.PRIORITY_NORMAL
        wwr.goal.requester = self.name
        wwr.goal.id = 0
        wwr.orientation.yaw = new_yaw
        wwr.disable_axis.x = True
        wwr.disable_axis.y = True
        wwr.disable_axis.z = True
        wwr.disable_axis.roll = True
        wwr.disable_axis.pitch = True
        wwr.disable_axis.yaw = False

        # Publish topic
        self.pub_wwr.publish(wwr)


if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('publish_subscribe_example')

        # Instatiated the class
        NC = Example(rospy.get_name())

        # Wait
        rospy.spin()
    except rospy.ROSInterruptException:
        pass

