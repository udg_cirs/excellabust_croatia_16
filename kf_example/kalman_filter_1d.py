#!/usr/bin/env python

#########################################################################
# Created by Narcis Palomeras, based on Greg Czerniak's implementation #
#########################################################################

import random
import numpy as np
import matplotlib.pyplot as plt
import math

# Implements a linear Kalman filter.
class KalmanFilterLinear:
    def __init__(self, _A, _H, _x, _P, _Q, _R):
        self.A = _A                      # State transition matrix.
        self.H = _H                      # Observation matrix.
        self.current_state_estimate = _x # Initial state estimate.
        self.current_prob_estimate = _P  # Initial covariance estimate.
        self.Q = _Q                      # Estimated error in process.
        self.R = _R                      # Estimated error in measurements.
        
    def GetCurrentState(self):
        return self.current_state_estimate
        
    def Step(self, measurement_vector):
        #---------------------------Prediction step-----------------------------
        predicted_state_estimate = self.A * self.current_state_estimate
        predicted_prob_estimate = (self.A * self.current_prob_estimate) * self.A.transpose() + self.Q
        
        #--------------------------Observation step-----------------------------
        innovation = measurement_vector - self.H*predicted_state_estimate
        innovation_covariance = self.H*predicted_prob_estimate*self.H.transpose() + self.R
        
        #-----------------------------Update step-------------------------------
        kalman_gain = predicted_prob_estimate * self.H.transpose() * np.linalg.inv(innovation_covariance)
        self.current_state_estimate = predicted_state_estimate + kalman_gain * innovation
        # We need the size of the matrix so we can make an identity matrix.
        size = self.current_prob_estimate.shape[0]
        # eye(n) = nxn identity matrix.
        self.current_prob_estimate = (np.eye(size)-kalman_gain*self.H)*predicted_prob_estimate
        


class Simulation1D:
    def __init__(self, p_init, v_init, a_inc, z_variance):
        self.p = p_init
        self.v = v_init
        self.a_inc = a_inc
        self.z_variance = z_variance
        self.current_a = 0.0
      
    def iteration(self, dt):
        self.current_a = self.current_a + self.a_inc * dt
        self.v = self.v + self.current_a * dt
        self.p = self.p + self.v * dt

    def get_position_measurement(self):
        return (self.p + random.gauss(0.0, math.sqrt(self.z_variance)))

    def get_real_position(self):
        return self.p



if __name__ == '__main__':
    
    dt = 1.0
    initial_position = 0.0
    initial_velocity = -2.0
    acceleration_inc = 0.001
    measure_variance = 5
    estimated_measure_variance = 5
    model_variance_position = 0.75
    model_variance_velocity = 0.2
    
    # No velocity model
    x = np.array([initial_position])
    P = np.identity(1)
    A = np.matrix([1])
    H = np.matrix([1])
    q = np.matrix([model_variance_position]) # process error
    Q = q.transpose()*q
    r = np.matrix([estimated_measure_variance]) # measurement error
    R = r.transpose()*r
    
    # Constant velocity model
    """
    x = np.array([initial_position, initial_velocity]).reshape(2,1)
    P = np.identity(2)
    A = np.matrix([1, dt, 0, 1]).reshape(2,2)
    H = np.matrix([1, 0])
    q = np.matrix([model_variance_position, model_variance_velocity]) # process error
    Q = q.transpose()*q
    r = np.matrix([estimated_measure_variance]) # measurement error
    R = r.transpose()*r 
    """
    
    # Create Simulation
    sim = Simulation1D(initial_position, initial_velocity, acceleration_inc, measure_variance)

    # Create Kalman Filter
    kf = KalmanFilterLinear(A, H, x, P, Q, R)

    # Simulate    
    real_p = []
    estimated_p = []
    measured_p = []
    
    time = range(100)
    for i in time:
        sim.iteration(dt);
        real_p.append(sim.get_real_position())
        z = sim.get_position_measurement()
        measured_p.append(z)
        kf.Step(z)
        estimated_p.append(float(kf.GetCurrentState()[0]))
    
    # Plot data
    plt.plot(time, real_p, 'g')
    plt.plot(time, estimated_p, 'r')
    plt.plot(time, measured_p, 'b*')
    plt.show()
